
# FROM nvidia/cuda:11.3.1-cudnn8-devel-ubuntu20.04
FROM nvidia/cuda:12.4.1-cudnn-devel-ubuntu20.04

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y libglib2.0-0 libxrender1 libsm6 libxext6 libxrender-dev vim wget sudo psmisc locales cmake vim g++ zip \
     htop git screen git-lfs gnupg libgl1 bwm-ng iputils-ping dnsutils curl sysstat axel rsync
# nvidia-cuda-toolkit
RUN locale-gen en_US.UTF-8

# Install miniconda
ENV CONDA_DIR /opt/conda
RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/conda
    
# Put conda in path so we can use conda activate
ENV PATH=$CONDA_DIR/bin:$PATH


# ADD https://raw.githubusercontent.com/ChengJiacheng/nautilus/master/yaml/torch19.yml /tmp/environment.yml
# RUN conda env create --file /tmp/environment.yml && conda init bash && echo "source activate base" >> ~/.bashrc
RUN conda create -n pytorch python=3.8 -y && conda init bash && echo "conda activate pytorch" >> ~/.bashrc

ENV PATH /opt/conda/envs/pytorch/bin:$PATH

RUN pip install torch torchvision torchaudio --no-cache-dir


RUN yes | pip install xtcocotools wandb

# ADD https://raw.githubusercontent.com/TexasInstruments/edgeai-yolov5/yolo-pose/requirements.txt ./ 
# RUN pip install -r requirements.txt

# RUN git clone https://github.com/Jeff-sjtu/CrowdPose.git && cd CrowdPose/crowdpose-api/PythonAPI/ && make install && python setup.py install --user

